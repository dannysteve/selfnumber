<?php

$notSelfNumbers = array();
$allNumbers = array();

for($i=1; $i<=5000; $i++)
{
	$b=$i;
	$b= (string)$b ;
	$allNumbers[] = $i;
	$number = strlen($i);
	if ($number == 1)
    {
		$sumOfThisNumbers = $i + $b[0];
		$notSelfNumbers[] = $sumOfThisNumbers;
	} elseif ($number == 2)
    {
		$sumOfThisNumbers = $i + $b[0] + $b[1];
		$notSelfNumbers[] = $sumOfThisNumbers;
	} elseif ($number == 3)
    {
		$sumOfThisNumbers = $i + $b[0] + $b[1] + $b[2];
		$notSelfNumbers[] = $sumOfThisNumbers;
	} elseif ($number == 4)
    {
		$sumOfThisNumbers = $i + $b[0] + $b[1] + $b[2] + $b[3];
		$notSelfNumbers[] = $sumOfThisNumbers;
	}
}

$selfNumbers = array_diff($allNumbers, $notSelfNumbers);
$sumSelfNumbers = 0;
$numbering = 1;
foreach($selfNumbers as $eachSelfNumber)
{
	echo "<pre>". $numbering++ .". ".$eachSelfNumber;
	$sumSelfNumbers+= $eachSelfNumber;
}

echo "<pre>Sum of all self-numbers from bigger than 1 and smaller than 5000 is: <strong>" . number_format($sumSelfNumbers)."</strong>";
?>
