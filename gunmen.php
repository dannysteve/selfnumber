<?php

$GLOBALS['board'] = [];

$GLOBALS['n'] = 8;
$GLOBALS['empty'] = 0;
$GLOBALS['rook'] = 0;
$GLOBALS['wall'] = 0;

    function can_place ($i, $j) {
        if ($GLOBALS['board'][$i][$j] != $GLOBALS['empty']) {
            return false;
        }
        $row = $j+1;
        while ($GLOBALS['board'][$i][$row] != $GLOBALS['wall'] && $row < $GLOBALS['n']) if ($GLOBALS['board'][$i][$row++] == $GLOBALS['rook']) return false;
        $row = $j-1;
        while ($GLOBALS['board'][$i][$row] != $GLOBALS['wall'] and $row >= 0) if ($GLOBALS['board'][$i][$row--] == $GLOBALS['rook']) return false;
        $col = $i+1;
        while ($GLOBALS['board'][$col][$j] != $GLOBALS['wall'] and $col < $GLOBALS['n']) if ($GLOBALS['board'][$col++][$j] == $GLOBALS['rook']) return false;
        $col = $i-1;
        while ($GLOBALS['board'][$col][$j] != $GLOBALS['wall'] and $col >= 0) if ($GLOBALS['board'][$col--][$j] == $GLOBALS['rook']) return false;
        return true;
    }

    function place() {
      $max_rooks = 0;
      for ($i = 0; $i < $GLOBALS['n']; $i++)
      for ($j = 0; $j < $GLOBALS['n']; $j++)
        if (can_place($i, $j)) {
          $oard[$i][$j] = $GLOBALS['rook'];
          $rooks = 1 + place();
          $max_rooks = max($max_rooks, $rooks);
          $GLOBALS['board'][$i][$j] = $GLOBALS['empty'];
        }
      return $max_rooks;
    }


    for ($i = 0; $i < $GLOBALS['n']; $i++)
    for ($j = 0; $j < $GLOBALS['n']; $j++) {
        /*
        yang ada wall

        row 1:
        [0][1]
        [0][3]
        [0][5]
        [0][7]

        row 2:
        [1][5]

        row 3:
        [2][0]
        [2][2]
        [2][5]
        [2][7]

        row 5:
        [4][3]

        row 6:
        [5][5]
        [5][7]

        row 7:
        [6][1]

        row 8:
        [7][4]
        [7][6]

        */
        if (
            ($i == 0 && $j == 1) ||
            ($i == 0 && $j == 3) ||
            ($i == 0 && $j == 5) ||
            ($i == 0 && $j == 7) ||
            ($i == 1 && $j == 5) ||
            ($i == 2 && $j == 0) ||
            ($i == 2 && $j == 2) ||
            ($i == 2 && $j == 5) ||
            ($i == 2 && $j == 7) ||
            ($i == 4 && $j == 3) ||
            ($i == 5 && $j == 5) ||
            ($i == 5 && $j == 7) ||
            ($i == 6 && $j == 1) ||
            ($i == 7 && $j == 4) ||
            ($i == 7 && $j == 6)
        ) {
            $GLOBALS['board'][$i][$j] = $GLOBALS['wall'];
        }
    }

    if ($GLOBALS['n'] != 0) {
        echo place();
        echo "<br/>";
        echo "<br/>";
    }



?>